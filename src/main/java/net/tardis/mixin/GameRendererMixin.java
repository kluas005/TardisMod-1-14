//package net.tardis.mixin;
//
//import com.mojang.blaze3d.matrix.MatrixStack;
//import net.minecraft.client.Minecraft;
//import net.minecraft.client.renderer.GameRenderer;
//import net.minecraft.client.renderer.WorldRenderer;
//import net.minecraft.util.math.vector.Matrix4f;
//import net.minecraftforge.common.MinecraftForge;
//import net.tardis.api.events.TardisWorldLastRender;
//import org.spongepowered.asm.mixin.Mixin;
//import org.spongepowered.asm.mixin.injection.At;
//import org.spongepowered.asm.mixin.injection.Inject;
//import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;
//
//@Mixin(GameRenderer.class)
//public class GameRendererMixin {
//
//    @Inject(at = @At("RETURN"), method = "renderWorld(FJLcom/mojang/blaze3d/matrix/MatrixStack;)V")
//    private void renderWorld(float partialTicks, long finishTimeNano, MatrixStack matrixStackIn, CallbackInfo callbackInfo) {
//    	
//    	//matrixStackIn.pop();
//    	
//    	Matrix4f matrix4f = matrixStackIn.getLast().getMatrix();
//        postEventAfterHands(Minecraft.getInstance().worldRenderer, matrixStackIn, partialTicks, matrix4f, finishTimeNano);
//    }
//
//    
//    public void postEventAfterHands(WorldRenderer context, MatrixStack mat, float partialTicks, Matrix4f projectionMatrix, long finishTimeNano){
//        MinecraftForge.EVENT_BUS.post(new TardisWorldLastRender(context, mat, partialTicks, projectionMatrix, finishTimeNano));
//    }
//
//}
