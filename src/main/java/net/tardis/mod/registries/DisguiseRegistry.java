package net.tardis.mod.registries;

import java.util.Optional;
import java.util.function.BiFunction;
import java.util.function.Supplier;

import net.minecraft.block.Blocks;
import net.minecraft.block.DoorBlock;
import net.minecraft.state.properties.DoubleBlockHalf;
import net.minecraft.util.RegistryKey;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.registry.Registry;
import net.minecraft.world.World;
import net.minecraft.world.biome.Biome.Category;
import net.minecraft.world.biome.Biomes;
import net.minecraft.world.gen.feature.StructureFeature;
import net.minecraft.world.gen.feature.structure.Structure;
import net.minecraft.world.server.ServerWorld;
import net.minecraftforge.fml.RegistryObject;
import net.minecraftforge.registries.DeferredRegister;
import net.minecraftforge.registries.IForgeRegistry;
import net.minecraftforge.registries.RegistryBuilder;
import net.tardis.mod.Tardis;
import net.tardis.mod.blocks.TBlocks;
import net.tardis.mod.misc.Disguise;
import net.tardis.mod.world.dimensions.TDimensions;

public class DisguiseRegistry {
	
    public static final DeferredRegister<Disguise> DISGUISES = DeferredRegister.create(Disguise.class, Tardis.MODID);
	
	public static Supplier<IForgeRegistry<Disguise>> DISGUISE_REGISTRY = DISGUISES.makeRegistry("disguise", () -> new RegistryBuilder<Disguise>().setMaxID(Integer.MAX_VALUE - 1));
    
	public static BiFunction<World, BlockPos, Boolean> DEFAULT_TREE_CRITERON = (world, pos) -> world.canBlockSeeSky(pos);// && world.isSurfaceWorld();
    public static BiFunction<World, BlockPos, Boolean> coral_criteron = (world, pos) -> world.func_242406_i(pos) == Optional.of(Biomes.WARM_OCEAN);
	public static BiFunction<World, BlockPos, Boolean> nether = (world, pos) -> world.getDimensionKey() == World.THE_NETHER;
	public static BiFunction<World, BlockPos, Boolean> MOON_CRITERON = (world, pos) -> world.getDimensionKey().getLocation().equals(TDimensions.MOON_DIM.getRegistryName());
    public static BiFunction<World, BlockPos, Boolean> DESERT_VILLAGE_CRITERION = (world, pos) -> {
    	if (!world.isRemote()) {
		    ServerWorld serverWorld = (ServerWorld) world;
    	    RegistryKey<StructureFeature<?, ?>> structureKey = RegistryKey.getOrCreateKey(Registry.CONFIGURED_STRUCTURE_FEATURE_KEY, new ResourceLocation("village_desert"));
    	    Structure<?> structureToSearch = serverWorld.getServer().getDynamicRegistries().getRegistry(Registry.CONFIGURED_STRUCTURE_FEATURE_KEY).getOrThrow(structureKey).field_236268_b_;
    	    BlockPos villagePos = serverWorld.getStructureLocation(structureToSearch, pos, 1, false);
		    return world.getBiome(pos).getCategory() == Category.DESERT && villagePos != null;
	    }
	    return false;
	};

    public static final RegistryObject<Disguise> OAK_TREE = DISGUISES.register("oak_tree", () -> createDisguise(new Disguise(() -> Blocks.OAK_LOG.getDefaultState()), (world, pos) -> (world.getBiome(pos).getCategory() == Category.FOREST || world.getBiome(pos).getCategory() == Category.PLAINS) && DEFAULT_TREE_CRITERON.apply(world, pos)));
    public static final RegistryObject<Disguise> CACTUS = DISGUISES.register("cactus", () -> createDisguise(new Disguise(() -> Blocks.CACTUS.getDefaultState()), (world, pos) -> world.getBiome(pos).getCategory() == Category.DESERT && DEFAULT_TREE_CRITERON.apply(world, pos)));
    public static final RegistryObject<Disguise> CACTUS_TALL = DISGUISES.register("cactus_tall", () -> createDisguise(new Disguise(() -> Blocks.CACTUS.getDefaultState()), (world, pos) -> world.getBiome(pos).getCategory() == Category.DESERT && DEFAULT_TREE_CRITERON.apply(world, pos)));
    public static final RegistryObject<Disguise> DESERT_LIBRARY_1 = DISGUISES.register("desert_library_1", () -> createDisguise(new Disguise(() -> Blocks.SPRUCE_DOOR.getDefaultState().with(DoorBlock.HALF, DoubleBlockHalf.UPPER), () -> Blocks.SPRUCE_DOOR.getDefaultState()), (world, pos) -> DESERT_VILLAGE_CRITERION.apply(world, pos)));
    public static final RegistryObject<Disguise> DESERT_SMALL_HOUSE_3 = DISGUISES.register("desert_small_house_3", () -> createDisguise(new Disguise(() -> TBlocks.exterior_disguise.get().getDefaultState(), () -> TBlocks.bottom_exterior.get().getDefaultState()), (world, pos) -> DESERT_VILLAGE_CRITERION.apply(world, pos)));
    public static final RegistryObject<Disguise> DESERT_MEDIUM_HOUSE_1 = DISGUISES.register("desert_medium_house_1", () -> createDisguise(new Disguise(() -> TBlocks.exterior_disguise.get().getDefaultState(), () -> TBlocks.bottom_exterior.get().getDefaultState()), (world, pos) -> DESERT_VILLAGE_CRITERION.apply(world, pos)));
    public static final RegistryObject<Disguise> STONE_PILLAR = DISGUISES.register("stone", () -> createDisguise(new Disguise(() -> Blocks.STONE.getDefaultState()), (world, pos) -> false));
    public static final RegistryObject<Disguise> BROWN_MUSHROOM_SMALL = DISGUISES.register("brown_mushroom_small", () -> createDisguise(new Disguise(() -> Blocks.MUSHROOM_STEM.getDefaultState()), (world, pos) -> world.getBiome(pos).getCategory() == Category.MUSHROOM && DEFAULT_TREE_CRITERON.apply(world, pos)));
    public static final RegistryObject<Disguise> BROWN_MUSHROOM_MEDIUM = DISGUISES.register("brown_mushroom_medium", () -> createDisguise(new Disguise(() -> Blocks.MUSHROOM_STEM.getDefaultState()), (world, pos) -> world.getBiome(pos).getCategory() == Category.MUSHROOM && DEFAULT_TREE_CRITERON.apply(world, pos)));
    public static final RegistryObject<Disguise> BROWN_MUSHROOM_BIG = DISGUISES.register("brown_mushroom_big", () -> createDisguise(new Disguise(() -> Blocks.MUSHROOM_STEM.getDefaultState()), (world, pos) -> world.getBiome(pos).getCategory() == Category.MUSHROOM && DEFAULT_TREE_CRITERON.apply(world, pos)));
    public static final RegistryObject<Disguise> SPRUCE_TREE = DISGUISES.register("spruce_tree", () -> createDisguise(new Disguise(() -> Blocks.SPRUCE_LOG.getDefaultState()), (world, pos) -> world.getBiome(pos).getCategory() == Category.TAIGA && DEFAULT_TREE_CRITERON.apply(world, pos)));
    public static final RegistryObject<Disguise> SPRUCE_TREE_A = DISGUISES.register("spruce_tree_a", () -> createDisguise(new Disguise(() -> Blocks.SPRUCE_LOG.getDefaultState()), (world, pos) -> world.getBiome(pos).getCategory() == Category.TAIGA && DEFAULT_TREE_CRITERON.apply(world, pos)));
    public static final RegistryObject<Disguise> SPRUCE_TREE_B = DISGUISES.register("spruce_tree_b", () -> createDisguise(new Disguise(() -> Blocks.SPRUCE_LOG.getDefaultState()), (world, pos) -> world.getBiome(pos).getCategory() == Category.TAIGA && DEFAULT_TREE_CRITERON.apply(world, pos)));
    public static final RegistryObject<Disguise> SPRUCE_TREE_C = DISGUISES.register("spruce_tree_c", () -> createDisguise(new Disguise(() -> Blocks.SPRUCE_LOG.getDefaultState()), (world, pos) -> world.getBiome(pos).getCategory() == Category.TAIGA && DEFAULT_TREE_CRITERON.apply(world, pos)));
    public static final RegistryObject<Disguise> SPRUCE_TREE_D = DISGUISES.register("spruce_tree_d", () -> createDisguise(new Disguise(() -> Blocks.SPRUCE_LOG.getDefaultState()), (world, pos) -> world.getBiome(pos).getCategory() == Category.TAIGA && DEFAULT_TREE_CRITERON.apply(world, pos)));
    public static final RegistryObject<Disguise> SPRUCE_TREE_E = DISGUISES.register("spruce_tree_e", () -> createDisguise(new Disguise(() -> Blocks.SPRUCE_LOG.getDefaultState()), (world, pos) -> world.getBiome(pos).getCategory() == Category.TAIGA && DEFAULT_TREE_CRITERON.apply(world, pos)));
    public static final RegistryObject<Disguise> BIRCH_TREE = DISGUISES.register("birch_tree", () -> createDisguise(new Disguise(() -> Blocks.BIRCH_LOG.getDefaultState()), (world, pos) -> (world.getBiome(pos).getCategory() == Category.PLAINS || world.getBiome(pos).getRegistryName().getPath().contains("birch")) && DEFAULT_TREE_CRITERON.apply(world, pos)));
    public static final RegistryObject<Disguise> BIRCH_TREE_A = DISGUISES.register("birch_tree_a", () -> createDisguise(new Disguise(() -> Blocks.BIRCH_LOG.getDefaultState()), (world, pos) -> (world.getBiome(pos).getCategory() == Category.PLAINS || world.getBiome(pos).getRegistryName().getPath().contains("birch")) && DEFAULT_TREE_CRITERON.apply(world, pos)));
    public static final RegistryObject<Disguise> BIRCH_TREE_B = DISGUISES.register("birch_tree_b", () -> createDisguise(new Disguise(() -> Blocks.BIRCH_LOG.getDefaultState()), (world, pos) -> (world.getBiome(pos).getCategory() == Category.PLAINS || world.getBiome(pos).getRegistryName().getPath().contains("birch")) && DEFAULT_TREE_CRITERON.apply(world, pos)));
    public static final RegistryObject<Disguise> BIRCH_TREE_C = DISGUISES.register("birch_tree_c", () -> createDisguise(new Disguise(() -> Blocks.BIRCH_LOG.getDefaultState()), (world, pos) -> (world.getBiome(pos).getCategory() == Category.PLAINS || world.getBiome(pos).getRegistryName().getPath().contains("birch")) && DEFAULT_TREE_CRITERON.apply(world, pos)));
    public static final RegistryObject<Disguise> BIRCH_TREE_D = DISGUISES.register("birch_tree_d", () -> createDisguise(new Disguise(() -> Blocks.BIRCH_LOG.getDefaultState()), (world, pos) -> (world.getBiome(pos).getCategory() == Category.PLAINS || world.getBiome(pos).getRegistryName().getPath().contains("birch")) && DEFAULT_TREE_CRITERON.apply(world, pos)));
    public static final RegistryObject<Disguise> BIRCH_TREE_E = DISGUISES.register("birch_tree_e", () -> createDisguise(new Disguise(() -> Blocks.BIRCH_LOG.getDefaultState()), (world, pos) -> (world.getBiome(pos).getCategory() == Category.PLAINS || world.getBiome(pos).getRegistryName().getPath().contains("birch")) && DEFAULT_TREE_CRITERON.apply(world, pos)));
    public static final RegistryObject<Disguise> BIRCH_TREE_F = DISGUISES.register("birch_tree_f", () -> createDisguise(new Disguise(() -> Blocks.BIRCH_LOG.getDefaultState()), (world, pos) -> (world.getBiome(pos).getCategory() == Category.PLAINS || world.getBiome(pos).getRegistryName().getPath().contains("birch")) && DEFAULT_TREE_CRITERON.apply(world, pos)));
    public static final RegistryObject<Disguise> ACACIA_TREE = DISGUISES.register("acacia_tree", () -> createDisguise(new Disguise(() -> Blocks.ACACIA_LOG.getDefaultState()), (world, pos) -> world.getBiome(pos).getCategory() == Category.SAVANNA && DEFAULT_TREE_CRITERON.apply(world, pos)));
    public static final RegistryObject<Disguise> ACACIA_TREE_A = DISGUISES.register("acacia_tree_a", () -> createDisguise(new Disguise(() -> Blocks.ACACIA_LOG.getDefaultState()), (world, pos) -> world.getBiome(pos).getCategory() == Category.SAVANNA && DEFAULT_TREE_CRITERON.apply(world, pos)));
    public static final RegistryObject<Disguise> ACACIA_TREE_B = DISGUISES.register("acacia_tree_b", () -> createDisguise(new Disguise(() -> Blocks.ACACIA_LOG.getDefaultState()), (world, pos) -> world.getBiome(pos).getCategory() == Category.SAVANNA && DEFAULT_TREE_CRITERON.apply(world, pos)));
    public static final RegistryObject<Disguise> ACACIA_TREE_C = DISGUISES.register("acacia_tree_c", () -> createDisguise(new Disguise(() -> Blocks.ACACIA_LOG.getDefaultState()), (world, pos) -> world.getBiome(pos).getCategory() == Category.SAVANNA && DEFAULT_TREE_CRITERON.apply(world, pos)));
    public static final RegistryObject<Disguise> ACACIA_TREE_D = DISGUISES.register("acacia_tree_d", () -> createDisguise(new Disguise(() -> Blocks.ACACIA_LOG.getDefaultState()), (world, pos) -> world.getBiome(pos).getCategory() == Category.SAVANNA && DEFAULT_TREE_CRITERON.apply(world, pos)));
    public static final RegistryObject<Disguise> ACACIA_TREE_E = DISGUISES.register("acacia_tree_e", () -> createDisguise(new Disguise(() -> Blocks.ACACIA_LOG.getDefaultState()), (world, pos) -> world.getBiome(pos).getCategory() == Category.SAVANNA && DEFAULT_TREE_CRITERON.apply(world, pos)));
    public static final RegistryObject<Disguise> CORAL_RED = DISGUISES.register("coral_fire", () -> createDisguise(new Disguise(() -> Blocks.FIRE_CORAL_BLOCK.getDefaultState()), coral_criteron));
    public static final RegistryObject<Disguise> CORAL_TUBE = DISGUISES.register("coral_tube", () -> createDisguise(new Disguise(() -> Blocks.TUBE_CORAL_BLOCK.getDefaultState()), coral_criteron));
    public static final RegistryObject<Disguise> CORAL_BRAIN = DISGUISES.register("coral_brain", () -> createDisguise(new Disguise(() -> Blocks.BRAIN_CORAL_BLOCK.getDefaultState()), coral_criteron));
    public static final RegistryObject<Disguise> CORAL_BUBBLE = DISGUISES.register("coral_bubble", () -> createDisguise(new Disguise(() -> Blocks.BUBBLE_CORAL_BLOCK.getDefaultState()), coral_criteron));
    public static final RegistryObject<Disguise> DEFAULT_OCEAN = DISGUISES.register("default_ocean", () -> createDisguise(new Disguise(() -> Blocks.SAND.getDefaultState()), coral_criteron));
    public static final RegistryObject<Disguise> NETHERRACK_PILLAR = DISGUISES.register("netherrack_pillar", () -> createDisguise(new Disguise(() -> Blocks.NETHERRACK.getDefaultState()), nether));
//    public static final RegistryObject<Disguise> NETHER_FORT = DISGUISES.register("nether_fort", () -> createDisguise(new Disguise(), nether));
    public static final RegistryObject<Disguise> SMALL_ROCKET = DISGUISES.register("rocket_small", () -> createDisguise(new Disguise(TBlocks.alabaster.get()::getDefaultState), MOON_CRITERON));

//	public static <T extends Disguise> T register(T dis, String name, BiFunction<World, BlockPos, Boolean> valid) {
//		dis.setRegistryName(new ResourceLocation(Tardis.MODID, name));
//		dis.setValidationFunction(valid);
//		return dis;
//	}
	
	public static <T extends Disguise> T createDisguise(T dis, BiFunction<World, BlockPos, Boolean> valid) {
		dis.setValidationFunction(valid);
		return dis;
	}

}
