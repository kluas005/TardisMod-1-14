package net.tardis.mod.registries;

import java.util.function.Supplier;

import net.minecraftforge.fml.RegistryObject;
import net.minecraftforge.registries.DeferredRegister;
import net.minecraftforge.registries.IForgeRegistry;
import net.minecraftforge.registries.RegistryBuilder;
import net.tardis.mod.Tardis;
import net.tardis.mod.sounds.InteriorHum;
import net.tardis.mod.sounds.TSounds;

public class InteriorHumRegistry {
	
    public static final DeferredRegister<InteriorHum> HUMS = DeferredRegister.create(InteriorHum.class, Tardis.MODID);
	
	public static Supplier<IForgeRegistry<InteriorHum>> HUM_REGISTRY = HUMS.makeRegistry("interior_hum", () -> new RegistryBuilder<InteriorHum>().setMaxID(Integer.MAX_VALUE - 1));
	
	public static final RegistryObject<InteriorHum> DISABLED = HUMS.register("disabled", () -> new InteriorHum(null, 0));
	public static final RegistryObject<InteriorHum> SIXTY_THREE = HUMS.register("63", () -> new InteriorHum(TSounds.TARDIS_HUM_63.get(), 50));
	public static final RegistryObject<InteriorHum> SEVENTY = HUMS.register("70", () -> new InteriorHum(TSounds.TARDIS_HUM_70.get(), 620));
	public static final RegistryObject<InteriorHum> EIGHTY = HUMS.register("80", () -> new InteriorHum(TSounds.TARDIS_HUM_80.get(), 600));
	public static final RegistryObject<InteriorHum> COPPER = HUMS.register("copper", () -> new InteriorHum(TSounds.TARDIS_HUM_COPPER.get(), 620));
	public static final RegistryObject<InteriorHum> CORAL = HUMS.register("coral", () -> new InteriorHum(TSounds.TARDIS_HUM_CORAL.get(), 1060));
	public static final RegistryObject<InteriorHum> TOYOTA = HUMS.register("toyota", () -> new InteriorHum(TSounds.TARDIS_HUM_TOYOTA.get(), 580));


}
