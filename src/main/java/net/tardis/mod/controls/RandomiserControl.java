package net.tardis.mod.controls;

import java.util.Random;

import net.minecraft.entity.EntitySize;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.util.SoundEvent;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.vector.Vector3d;
import net.tardis.mod.entity.ControlEntity;
import net.tardis.mod.registries.ControlRegistry.ControlEntry;
import net.tardis.mod.sounds.TSounds;
import net.tardis.mod.tileentities.ConsoleTile;
import net.tardis.mod.tileentities.consoles.CoralConsoleTile;
import net.tardis.mod.tileentities.consoles.GalvanicConsoleTile;
import net.tardis.mod.tileentities.consoles.HartnellConsoleTile;
import net.tardis.mod.tileentities.consoles.KeltConsoleTile;
import net.tardis.mod.tileentities.consoles.NemoConsoleTile;
import net.tardis.mod.tileentities.consoles.NeutronConsoleTile;
import net.tardis.mod.tileentities.consoles.ToyotaConsoleTile;
import net.tardis.mod.tileentities.consoles.XionConsoleTile;

public class RandomiserControl extends BaseControl{
	
	private static Random rand = new Random();
	
	public RandomiserControl(ControlEntry entry, ConsoleTile console, ControlEntity entity) {
		super(entry, console, entity);
	}

	@Override
	public EntitySize getSize() {
		
		if(this.getConsole() instanceof GalvanicConsoleTile)
			return EntitySize.flexible(0.099999994F, 0.099999994F);

		if(getConsole() instanceof CoralConsoleTile){
			return EntitySize.flexible(0.125F, 0.125F);
		}
		
		if(this.getConsole() instanceof HartnellConsoleTile)
			return EntitySize.flexible(0.125F, 0.125F);

		if(this.getConsole() instanceof ToyotaConsoleTile)
			return EntitySize.flexible(0.1875F, 0.1875F);
		
		if (this.getConsole() instanceof XionConsoleTile)
			return EntitySize.flexible(0.125F, 0.125F);
		
		if(this.getConsole() instanceof NeutronConsoleTile)
			return EntitySize.flexible(0.1875F, 0.1875F);
		
		if(this.getConsole() instanceof KeltConsoleTile)
			return EntitySize.flexible(0.18749999999999997F, 0.1875F);
		
		return EntitySize.flexible(4 / 16.0F, 4 / 16.0F);
	}
	
	@Override
	public Vector3d getPos() {
		ConsoleTile console = this.getConsole();
		if(console instanceof NemoConsoleTile)
			return new Vector3d(-7 / 16.0, 8 / 16.0, -4 / 16.0);

		if(console instanceof GalvanicConsoleTile)
			return new Vector3d(0.556702295430046, 0.45615, 0.5865187044643609);

		if(console instanceof CoralConsoleTile){
			return new Vector3d(-0.44420349584977736, 0.46875, 0.525);
		}
		
		if(console instanceof HartnellConsoleTile)
			return new Vector3d(-0.606, 0.469, -0.11);
		
		if(console instanceof ToyotaConsoleTile)
			return new Vector3d(-0.817, 0.438, 0.208);
		
		if (this.getConsole() instanceof XionConsoleTile)
			return new Vector3d(0.888690708649059, 0.28125, -0.3206850653868949);
		
		if(this.getConsole() instanceof NeutronConsoleTile)
			return new Vector3d(-0.012891798795441556, 0.375, 1.1199373433939346);
		
		if(this.getConsole() instanceof KeltConsoleTile)
			return new Vector3d(-0.3369791532518559, 0.3125, 0.9700882063031067);
		
		return new Vector3d(0, 12 / 16.0, 9 / 16.0);
	}


	@Override
	public boolean onRightClicked(ConsoleTile console, PlayerEntity player) {
		
		if(!player.world.isRemote && console.getLandTime() <= 0) {
			int rad = 5 * console.coordIncr;
			BlockPos dest = console.getDestinationPosition().add(rad - rand.nextInt(rad * 2), 0, rad - rand.nextInt(rad * 2));
			console.setDestination(console.getDestinationDimension(), dest);
			this.startAnimation();
		}
		return true;
	}

	@Override
	public SoundEvent getFailSound(ConsoleTile console) {
		return TSounds.SINGLE_CLOISTER.get();
	}

	@Override
	public SoundEvent getSuccessSound(ConsoleTile console) {
		return TSounds.RANDOMISER.get();
	}

	@Override
	public void deserializeNBT(CompoundNBT tag) {}
	
	@Override
	public CompoundNBT serializeNBT() {
		return new CompoundNBT();
	}
}
