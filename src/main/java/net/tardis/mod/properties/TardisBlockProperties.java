package net.tardis.mod.properties;

import net.minecraft.state.IntegerProperty;

public class TardisBlockProperties {

    public static final IntegerProperty LIGHT = IntegerProperty.create("light", 0, 15);
}
