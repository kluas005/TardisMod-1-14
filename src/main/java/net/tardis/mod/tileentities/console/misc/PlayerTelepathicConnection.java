package net.tardis.mod.tileentities.console.misc;

import java.util.Optional;
import java.util.UUID;

import javax.annotation.Nullable;

import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.ServerPlayerEntity;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.text.TranslationTextComponent;
import net.minecraftforge.common.util.INBTSerializable;
import net.tardis.mod.tileentities.ConsoleTile;
/** Wrapper object that defines a player by UUID and logic to execute when certain conditions are met*/
public class PlayerTelepathicConnection implements INBTSerializable<CompoundNBT>{

	private ConsoleTile tile;
	public UUID player;
	public int connectedTicks;
	
	public PlayerTelepathicConnection(ConsoleTile tile, UUID id, int connectedTicks) {
		this.player = id;
		this.connectedTicks = connectedTicks;
		this.tile = tile;
	}
	
	public PlayerTelepathicConnection(ConsoleTile tile, PlayerEntity player, int connectedTicks) {
		this(tile, player.getUniqueID(), connectedTicks);
	}
	
	public PlayerTelepathicConnection(ConsoleTile tile, CompoundNBT tag) {
		this.tile = tile;
		this.deserializeNBT(tag);
	}
	
	public boolean decrementAndCheckRemove(MinecraftServer server) {
		--this.connectedTicks;
		if(this.connectedTicks <= 0) {
			this.onDisconnected(server);
			return true;
		}
		return false;
	}
	/** What should be done when the connected player is disconnected from the telepathic circuit*/
	public void onDisconnected(MinecraftServer server) {
		this.getPlayer(server).ifPresent(player -> {
			player.sendStatusMessage(new TranslationTextComponent("message.tardis.telepathic.not_connected"), false);
		});
	}
	/** What should be done when the connected player is sleeping in a bed in the Tardis*/
	public void onSlept(MinecraftServer server) {
		this.getPlayer(server).ifPresent(player -> {
			if(this.tile != null)
				player.sendStatusMessage(tile.getEmotionHandler().getMoodState().getTranslationComponent(), false);
		});
	}

	public Optional<ServerPlayerEntity> getPlayer(MinecraftServer server) {

		if(server == null || this.player == null)
			return Optional.empty();

		ServerPlayerEntity player = server.getPlayerList().getPlayerByUUID(this.player);
		if(player == null)
			return Optional.empty();

		return Optional.of(player);
	}

	@Override
	public CompoundNBT serializeNBT() {
		CompoundNBT tag = new CompoundNBT();
		tag.putUniqueId("id", this.player);
		tag.putInt("ticks", this.connectedTicks);
		return tag;
	}

	@Override
	public void deserializeNBT(CompoundNBT tag) {
		this.player = tag.getUniqueId("id");
		this.connectedTicks = tag.getInt("ticks");
	}

	public UUID getPlayerId() {
		return this.player;
	}
}
