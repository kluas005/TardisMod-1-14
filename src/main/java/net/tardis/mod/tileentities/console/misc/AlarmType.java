package net.tardis.mod.tileentities.console.misc;

import net.minecraft.util.SoundEvent;
import net.tardis.mod.sounds.TSounds;

public enum AlarmType implements IAlarmType{

	CLOISTER(TSounds.SINGLE_CLOISTER.get(), 70),
	LOW(TSounds.ALARM_LOW.get(), 40);
	
	SoundEvent type;
	int time;
	
	AlarmType(SoundEvent event, int time) {
		this.type = event;
		this.time = time;
	}

	@Override
	public SoundEvent getLoopSound() {
		return this.type;
	}

	@Override
	public int getLoopType() {
		return this.time;
	}
	
}
