package net.tardis.mod.tileentities.machines;

import net.minecraft.block.BlockState;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.network.NetworkManager;
import net.minecraft.network.play.server.SUpdateTileEntityPacket;
import net.minecraft.tileentity.ITickableTileEntity;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.tileentity.TileEntityType;
import net.minecraft.util.SoundCategory;
import net.tardis.mod.artron.IArtronItemStackBattery;
import net.tardis.mod.artron.IArtronHolder;
import net.tardis.mod.config.TConfig;
import net.tardis.mod.constants.TardisConstants;
import net.tardis.mod.network.Network;
import net.tardis.mod.particles.TParticleTypes;
import net.tardis.mod.sounds.TSounds;
import net.tardis.mod.tileentities.TTiles;

public class ArtronCollectorTile extends TileEntity implements ITickableTileEntity, IArtronHolder{

	private float artron = 0;
	ItemStack stack = ItemStack.EMPTY;
	//Client side variables, don't serialise them for now
	private int particleSpawnTimer = TardisConstants.BUBBLE_PARTICLE_MAX_AGE;
	private boolean hasSpawnedInitialParticle = false;
	
	public ArtronCollectorTile(TileEntityType<?> tileEntityTypeIn) {
		super(tileEntityTypeIn);
	}
	
	public ArtronCollectorTile() {
		this(TTiles.ARTRON_COLLECTOR.get());
	}

	@Override
	public void tick() {
		if (this.getItem().isEmpty()) {
			this.hasSpawnedInitialParticle = false;
		}
		if(!this.getItem().isEmpty() && world.isRemote()) {
			if (!this.hasSpawnedInitialParticle) { //Spawn initial particle
				world.addParticle(TParticleTypes.BUBBLE.get(), pos.getX() + 0.5, pos.getY() + 1.5, pos.getZ() + 0.5, 0, 0.0, 0);
				this.hasSpawnedInitialParticle = true;
			}
			//Start counting down the timer to spawn additional particles after the initial one was spawned
			if (particleSpawnTimer > 0) {
				--particleSpawnTimer;
			}
			if (this.particleSpawnTimer == 7) {
//				System.out.println("bubble spawned");
				world.addParticle(TParticleTypes.BUBBLE.get(), pos.getX() + 0.5, pos.getY() + 1.5, pos.getZ() + 0.5, 0, 0.0, 0);
				this.resetParticleSpawnTimer();
			}
		}
		if(!world.isRemote) {
			if(this.getItem().getItem() instanceof IArtronItemStackBattery) {
				if (world.getGameTime() % 20 == 0) {
			        float amountToTake = TConfig.SERVER.artronCollectorBatteryChargeRate.get();
			        float amountTaken = this.takeArtron(amountToTake);
			        ((IArtronItemStackBattery)this.getItem().getItem()).charge(this.getItem(), amountTaken);
				    this.markDirty();
				    if (world.getGameTime() % 60 == 0)
				        this.world.playSound(null, this.getPos(), TSounds.ELECTRIC_ARC.get(), SoundCategory.BLOCKS, 0.2F, 1F);
				}
			}
		}
	}
	
	public float getArtron() {
		return this.artron;
	}

	@Override
	public float recieveArtron(float amount) {
		this.artron += amount;
		return amount;
	}

	@Override
	public float takeArtron(float amt) {
		if(this.artron >= amt) {
			this.artron -= amt;
			return amt;
		}
		else {
			float oldArtron = this.artron;
			this.artron = 0;
			return oldArtron;
		}
	}

	@Override
	public void onDataPacket(NetworkManager net, SUpdateTileEntityPacket pkt) {
		super.onDataPacket(net, pkt);
		this.deserializeNBT(pkt.getNbtCompound());
	}

	@Override
	public void read(BlockState state, CompoundNBT compound) {
		super.read(state, compound);
		if(compound.contains("item"))
			this.stack = ItemStack.read(compound.getCompound("item"));
		this.artron = compound.getFloat("artron");
	}

	@Override
	public CompoundNBT write(CompoundNBT compound) {
		compound.put("item", this.stack.serializeNBT());
		compound.putFloat("artron", this.artron);
		return super.write(compound);
	}

	@Override
	public SUpdateTileEntityPacket getUpdatePacket() {
		return Network.createTEUpdatePacket(this);
	}

	@Override
	public void handleUpdateTag(BlockState state, CompoundNBT tag) {
		this.read(state, tag);
	}

	@Override
	public CompoundNBT getUpdateTag() {
		return this.serializeNBT();
	}
	
	public void update() {
		if(!world.isRemote)
			world.notifyBlockUpdate(getPos(), getBlockState(), getBlockState(), 3);
	}
	
	public void placeItem(ItemStack stack) {
		this.stack = stack;
	}
	
	public ItemStack getItem() {
		return this.stack;
	}
	
	public void resetParticleSpawnTimer() {
		this.particleSpawnTimer = TardisConstants.BUBBLE_PARTICLE_MAX_AGE;
	}

}
