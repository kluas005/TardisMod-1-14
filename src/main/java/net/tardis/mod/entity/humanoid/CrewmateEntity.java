package net.tardis.mod.entity.humanoid;

import net.minecraft.entity.CreatureEntity;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.ILivingEntityData;
import net.minecraft.entity.SpawnReason;
import net.minecraft.entity.ai.attributes.AttributeModifierMap;
import net.minecraft.entity.ai.attributes.Attributes;
import net.minecraft.entity.ai.goal.WaterAvoidingRandomWalkingGoal;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.inventory.EquipmentSlotType;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.util.ActionResultType;
import net.minecraft.util.Hand;
import net.minecraft.world.DifficultyInstance;
import net.minecraft.world.IServerWorld;
import net.minecraft.world.World;
import net.tardis.mod.entity.TEntities;
import net.tardis.mod.entity.ai.humanoids.DoCrewWorkGoal;
import net.tardis.mod.entity.ai.humanoids.SitInChairGoal;
import net.tardis.mod.entity.mission.HumanoidMissionEntity;
import net.tardis.mod.items.TItems;
import net.tardis.mod.missions.misc.Dialog;

public class CrewmateEntity extends HumanoidMissionEntity{

	
	public CrewmateEntity(EntityType<? extends CreatureEntity> type, World worldIn) {
		super(type, worldIn);
	}
	
	public CrewmateEntity(World worldIn) {
		super(TEntities.CREWMATE.get(), worldIn);
	}

	@Override
	public Dialog getCurrentDialog(PlayerEntity player) {
		return null;
	}

	/**
	 * processInteract equivalent
	 * <br> In 1.16.2+ this is essentially a fallback method that is being called if the vanilla logic is not being met on right click
	 */
	@Override
	public ActionResultType getEntityInteractionResult(PlayerEntity player, Hand hand) {
		return super.getEntityInteractionResult(player, hand);
	}

	@Override
	protected void registerGoals() {
		super.registerGoals();
		
		//Tasks
		this.goalSelector.addGoal(3, new SitInChairGoal(this, 0.2334, 16));
		this.goalSelector.addGoal(2, new DoCrewWorkGoal(this, 0.2334, 16));
		this.goalSelector.addGoal(8, new WaterAvoidingRandomWalkingGoal(this, 0.3D));
	}

	@Override
	public ILivingEntityData onInitialSpawn(IServerWorld worldIn, DifficultyInstance difficultyIn, SpawnReason reason, ILivingEntityData spawnDataIn, CompoundNBT dataTag) {
		setupDefaultEquipment();
		return super.onInitialSpawn(worldIn, difficultyIn, reason, spawnDataIn, dataTag);
	}

	public void setupDefaultEquipment() {
		if(!world.isRemote()) {
			this.setItemStackToSlot(EquipmentSlotType.FEET, new ItemStack(TItems.SPACE_BOOTS.get()));
		}
	}
	
	@Override
	protected void dropInventory() {
		for (ItemStack stack : this.getArmorInventoryList()) {
			this.entityDropItem(stack, 0.0F);
		}
	}
	
	public static AttributeModifierMap.MutableAttribute createAttributes() {
    	return CreatureEntity.func_233666_p_().createMutableAttribute(Attributes.MAX_HEALTH, 20D)
    			.createMutableAttribute(Attributes.ARMOR_TOUGHNESS, 10D);
    }
}
