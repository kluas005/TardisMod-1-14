package net.tardis.mod.items;

import java.util.List;

import net.minecraft.client.gui.screen.Screen;
import net.minecraft.client.util.ITooltipFlag;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.item.ItemUseContext;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.util.ActionResultType;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.text.IFormattableTextComponent;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.StringTextComponent;
import net.minecraft.util.text.TextComponent;
import net.minecraft.util.text.TranslationTextComponent;
import net.minecraft.world.World;
import net.tardis.mod.constants.TardisConstants;
import net.tardis.mod.helper.TextHelper;
import net.tardis.mod.helper.WorldHelper;
import net.tardis.mod.properties.Prop;

public class TapeMeasureItem extends Item {
	
	private final IFormattableTextComponent descriptionTooltip = TextHelper.createDescriptionItemTooltip(new TranslationTextComponent("tooltip.tape_measure.howto"));
	private final IFormattableTextComponent descriptionTooltipTwo = TextHelper.createExtraLineItemTooltip(new TranslationTextComponent("tooltip.tape_measure.howto_2"));
	
    public TapeMeasureItem() {
        super(Prop.Items.ONE.get().group(null));
    }

    @Override
    public void addInformation(ItemStack stack, World worldIn, List<ITextComponent> tooltip, ITooltipFlag flagIn) {
        CompoundNBT tag = stack.getTag();
        tooltip.add(new StringTextComponent("Tool for measuring block position differences"));
        tooltip.add(TardisConstants.Translations.TOOLTIP_HOLD_SHIFT);
        if (tag != null && !tag.isEmpty()) {
            if (tag.contains("first")) {
                BlockPos pos = BlockPos.fromLong(tag.getLong("first"));
                tooltip.add(new StringTextComponent("First: " + pos.getX() + ", " + pos.getY() + ", " + pos.getZ()));
            }

            if (tag.contains("second")) {
                BlockPos pos = BlockPos.fromLong(tag.getLong("second"));
                tooltip.add(new StringTextComponent("Second: " + pos.getX() + ", " + pos.getY() + ", " + pos.getZ()));
            }

            if (tag.contains("first") && tag.contains("second")) {
                BlockPos first = BlockPos.fromLong(tag.getLong("first"));
                BlockPos second = BlockPos.fromLong(tag.getLong("second"));

                int x = first.getX() > second.getX() ? first.getX() - second.getX() : second.getX() - first.getX();
                int y = first.getY() > second.getY() ? first.getY() - second.getY() : second.getY() - first.getY();
                int z = first.getZ() > second.getZ() ? first.getZ() - second.getZ() : second.getZ() - first.getZ();

                tooltip.add(new StringTextComponent("Diff: " + x + ", " + y + ", " + z));
            }
        }
        if (Screen.hasShiftDown()) {
            tooltip.clear();
            tooltip.add(0, this.getDisplayName(stack));
            tooltip.add(descriptionTooltip);
            tooltip.add(descriptionTooltipTwo);
        }
        super.addInformation(stack, worldIn, tooltip, flagIn);

    }

    @Override
    public ActionResultType onItemUseFirst(ItemStack stack, ItemUseContext context) {
    	if (!context.getWorld().isRemote()) {
    		if (context.getPlayer().isSneaking()) {
                context.getItem().setTag(new CompoundNBT());
                return ActionResultType.SUCCESS;
            }

            CompoundNBT nbt = context.getItem().getOrCreateTag();

            if (nbt.contains("first")) {
            	
            	BlockPos first = BlockPos.fromLong(nbt.getLong("first"));
                BlockPos second = context.getPos();
                nbt.putLong("second", second.toLong());
                
                int secondX = context.getPos().getX();
            	int secondY = context.getPos().getY();
            	int secondZ = context.getPos().getZ();
            	TextComponent secondPos = TextHelper.createTextComponentWithTip("x:" + secondX + " y:" + secondY + " z:" + secondZ, WorldHelper.formatBlockPosDebug(new BlockPos(secondX,secondY,secondZ)));
            	context.getPlayer().sendStatusMessage(new StringTextComponent("Second Position: ").appendSibling(secondPos), false);
                
                int x = first.getX() > second.getX() ? first.getX() - second.getX() : second.getX() - first.getX();
                int y = first.getY() > second.getY() ? first.getY() - second.getY() : second.getY() - first.getY();
                int z = first.getZ() > second.getZ() ? first.getZ() - second.getZ() : second.getZ() - first.getZ();

                TextComponent diffPos = TextHelper.createTextComponentWithTip("x:" + x + " y:" + y + " z:" + z, WorldHelper.formatBlockPosDebug(new BlockPos(x,y,z)));
            	context.getPlayer().sendStatusMessage(new StringTextComponent("BlockPos Difference: ").appendSibling(diffPos), false);      
            }
            else {
            	nbt.putLong("first", context.getPos().toLong());
            	int x = context.getPos().getX();
            	int y = context.getPos().getY();
            	int z = context.getPos().getZ();
            	TextComponent firstPos = TextHelper.createTextComponentWithTip("x:" + x + " y:" + y + " z:" + z, WorldHelper.formatBlockPosDebug(new BlockPos(x,y,z)));
            	context.getPlayer().sendStatusMessage(new StringTextComponent("First Position: ").appendSibling(firstPos), false);
            }

            context.getItem().setTag(nbt);
    	}
        return ActionResultType.SUCCESS;
    }

}
