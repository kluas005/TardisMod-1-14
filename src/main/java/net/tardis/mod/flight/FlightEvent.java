package net.tardis.mod.flight;

import java.util.List;
import java.util.Random;

import com.google.common.collect.Lists;

import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.ServerPlayerEntity;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.SoundCategory;
import net.minecraft.util.SoundEvents;
import net.minecraft.util.Util;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.text.TextFormatting;
import net.minecraft.util.text.TranslationTextComponent;
import net.minecraft.world.World;
import net.tardis.mod.controls.AbstractControl;
import net.tardis.mod.controls.ThrottleControl;
import net.tardis.mod.misc.ObjectWrapper;
import net.tardis.mod.network.Network;
import net.tardis.mod.network.packets.MissControlMessage;
import net.tardis.mod.subsystem.Subsystem;
import net.tardis.mod.tileentities.ConsoleTile;
/** Logic class that defines arbritary events that can occur during flight
 * <br> Made up of its registry entry and a list of registry names of {@linkplain AbstractControl} that must be pressed to complete the event
 * <br> Author: @Spectre0987 */
public abstract class FlightEvent {
    
	private FlightEventFactory entry;
	private List<ResourceLocation> controlsLeft;
	protected int timeUntilMiss = 0;
	private boolean isComplete = false;
	private String translationKey;
	/**
	 * @param entry - The registry entry that this flight event is bound to
	 * @param controls - Registry names of the controls required to complete the flight event
	 */
	public FlightEvent(FlightEventFactory entry, ResourceLocation... controls) {
		this(entry, Lists.newArrayList(controls));
	}
	
	public FlightEvent(FlightEventFactory entry, List<ResourceLocation> controls) {
		this.controlsLeft = controls;
		this.entry = entry;
	}

	/**
	 * 
	 * @param tile
	 * @return - True if completed successfully, false if missed
	 */
	public boolean onComplete(ConsoleTile tile) {
		if(!this.controlsLeft.isEmpty()) {
			this.onMiss(tile);
			
			Random rand = tile.getWorld().rand;
			
			tile.getWorld().playSound(null, tile.getPos(), SoundEvents.ENTITY_GENERIC_EXPLODE, SoundCategory.BLOCKS, 0.5F, 1F);
			for (LivingEntity ent : tile.getWorld().getEntitiesWithinAABB(LivingEntity.class, new AxisAlignedBB(tile.getPos()).grow(20))) {
				ent.setMotion(ent.getMotion().add(rand.nextDouble() - 0.5, rand.nextDouble(), rand.nextDouble() - 0.5));
				if (ent instanceof ServerPlayerEntity) {
					Network.sendTo(new MissControlMessage(), (ServerPlayerEntity) ent);
				}
			}
			tile.getEmotionHandler().setMood(tile.getEmotionHandler().getMood() - 10);
			tile.getEmotionHandler().addLoyalty(tile.getPilot(), -5);
			for(Subsystem s : tile.getSubSystems()) {
				s.explode(true);
			}
			return false;
		}
		tile.getEmotionHandler().addLoyalty(tile.getPilot(), 1);
		return true;
	}
	
	public void onMiss(ConsoleTile tile) {}
	
	public boolean onControlHit(ConsoleTile console, AbstractControl control) {
		if(this.controlsLeft.contains(control.getEntry().getRegistryName())) {
			this.controlsLeft.remove(control.getEntry().getRegistryName());
			if(this.controlsLeft.isEmpty()) {
				this.isComplete = true;
				console.updateClient();
			}
			return true;
		}
		return false;
	}
	
	public int calcTime(ConsoleTile console) {
		
		ObjectWrapper<Float> wrapper = new ObjectWrapper<Float>(0.0F);
		
		console.getControl(ThrottleControl.class).ifPresent(throt -> wrapper.setValue(throt.getAmount()));
		float amt = wrapper.getValue();
		
		//flight time + 5 seconds for base + 0-5 seconds depending on throttle
		return this.timeUntilMiss = console.flightTicks + (5 * 20) + (int)Math.floor((1.0F - amt) * 5 * 20);
	}
	
	public int getMissedTime() {
		return this.timeUntilMiss;
	}
	
	public List<ResourceLocation> getControls(){
		return this.controlsLeft;
	}
	
	public String getTranslationKey() {
		if (this.translationKey == null) {
			this.translationKey = Util.makeTranslationKey("flight_events", this.getEntry().getRegistryName());
		}
		return this.translationKey;
	}
	
	public TranslationTextComponent getTranslation() {
		return new TranslationTextComponent(this.getTranslationKey());
	}

	public static interface IFlightConsequence{
		void onMissed(ConsoleTile tile);
	}

	public void warnPlayers(World world, BlockPos pos) {
		if(!world.isRemote) {
			for(PlayerEntity player : world.getEntitiesWithinAABB(PlayerEntity.class, new AxisAlignedBB(pos).grow(26))) {
				player.sendStatusMessage(getTranslation().mergeStyle(TextFormatting.ITALIC), true);
			}
		}
	}
	
	public boolean isComplete() {
		return this.isComplete;
	}
	
	public FlightEventFactory getEntry() {
		return this.entry;
	}
    
}
