package net.tardis.mod.client.guis.monitors;

import com.mojang.blaze3d.matrix.MatrixStack;

import net.minecraft.client.Minecraft;
import net.minecraft.client.audio.SimpleSound;
import net.minecraft.util.ResourceLocation;
import net.tardis.mod.Tardis;
import net.tardis.mod.constants.TardisConstants;
import net.tardis.mod.sounds.TSounds;

public class XionMonitorScreen extends BaseMonitorScreen {

    public static final ResourceLocation TEXTURE = new ResourceLocation(Tardis.MODID, "textures/gui/monitors/xion.png");

    @Override
    public void renderMonitor(MatrixStack matrixStack) {
        Minecraft.getInstance().getTextureManager().bindTexture(TEXTURE);
        this.renderBackground(matrixStack);
        int texWidth = 239, texHeight = 182;
        this.blit(matrixStack, width / 2 - texWidth / 2, height / 2 - texHeight / 2, 0, 0, texWidth, texHeight);
    }

    @Override
    protected void init() {
        super.init();
        Minecraft.getInstance().getSoundHandler().play(SimpleSound.master(TSounds.EYE_MONITOR_INTERACT.get(), 1.0F));
    }

    @Override
    public int getMinY() {
        return this.height / 2 + 60;
    }

    @Override
    public int getMinX() {
        return this.width / 2 - 100;
    }

    @Override
    public int getMaxX() {
        return this.getMinX() + 200;
    }

    @Override
    public int getMaxY() {
        return this.getMinY() - 140;
    }

    @Override
    public int getWidth() {
        return 201;
    }

    @Override
    public int getHeight() {
        return 152;
    }

    @Override
    public int getGuiID() {
        return TardisConstants.Gui.MONITOR_MAIN_XION;
    }


}
