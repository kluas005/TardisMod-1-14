package net.tardis.mod.client.guis.containers;

import com.mojang.blaze3d.matrix.MatrixStack;

import net.minecraft.client.gui.screen.inventory.ContainerScreen;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.TranslationTextComponent;
import net.tardis.mod.Tardis;
import net.tardis.mod.constants.TardisConstants;
import net.tardis.mod.containers.AlembicContainer;
import net.tardis.mod.helper.Helper;
import net.tardis.mod.helper.TextHelper;
import net.tardis.mod.tileentities.machines.AlembicTile;

public class AlembicScreen extends ContainerScreen<AlembicContainer> {

    public static final ResourceLocation TEXTURE = new ResourceLocation(Tardis.MODID, "textures/gui/containers/alembic.png");
    private final TranslationTextComponent title = new TranslationTextComponent("container.tardis.alembic");
    private AlembicTile tile;

    public AlembicScreen(AlembicContainer cont, PlayerInventory inv, ITextComponent titleIn) {
        super(cont, inv, titleIn);
        tile = cont.getBlockEntity();
        this.xSize = 176;
        this.ySize = 166;
    }

    @Override
    protected void drawGuiContainerBackgroundLayer(MatrixStack matrixStack, float partialTicks, int x, int y) {
        this.renderBackground(matrixStack);
        this.minecraft.textureManager.bindTexture(TEXTURE);
        this.blit(matrixStack, width / 2 - xSize / 2, height / 2 - ySize / 2, 0, 0, xSize, ySize);

        //Water
        int waterScaled = (int) (53 * (1- (tile.getWaterPercentage())));
        int offsetWaterBlit = 53 - (53 - waterScaled);
        blit(matrixStack, width / 2 - 42, height / 2 - 73 + offsetWaterBlit, 193, 19 + offsetWaterBlit, 10, 53 - offsetWaterBlit);
        
        //Non Water tank
        int nonWaterScaled = (int) (53 * (1 - (tile.getNonWaterPercentage())));
        int offsetNonWaterFluidBlit = 53 - (53 - nonWaterScaled);
        blit(matrixStack, width / 2 + 31, height / 2 - 73 + offsetNonWaterFluidBlit, 207, 19 + offsetNonWaterFluidBlit, 10, 53 - offsetNonWaterFluidBlit);

        //Fire
        if (tile.getMaxBurnTime() > 0) {
            int fireScaled = (int) (13 * (1 - (tile.getBurnTimePercent())));
            blit(matrixStack, width / 2 - 25, height / 2 - 53 + fireScaled, 203, 1 + fireScaled, 13, 13 - fireScaled);
        }
        
        //Progress Bar
        blit(matrixStack, width / 2 + 2, height / 2 - 52, 178, 1, (int) (23 * tile.getProgressPercent()), 18);
        
        //Water level lines
        blit(matrixStack, width / 2 - 39, height / 2 - 71, 182, 21, 7, 49);
        
        //Non Water Tank level lines
        blit(matrixStack, width / 2 + 34, height / 2 - 71, 182, 21, 7, 49);
    }
    
    
    //drawLabels in Mojmap
    @Override
    protected void drawGuiContainerForegroundLayer(MatrixStack matrixStack, int x, int y) {
        this.font.drawText(matrixStack, this.playerInventory.getDisplayName(), (float)this.playerInventoryTitleX, (float)this.playerInventoryTitleY, 4210752);
    }

    @Override
    public void render(MatrixStack matrixStack, int mouseX, int mouseY, float partialTicks) {
        super.render(matrixStack, mouseX, mouseY, partialTicks);
        this.renderHoveredTooltip(matrixStack, mouseX, mouseY);
        drawCenteredString(matrixStack, this.font, title.getString(), this.width / 2, this.guiTop - 13, 0xFFFFFF);
        renderCustomToolTips(matrixStack, mouseX, mouseY);
    }
    
    protected void renderCustomToolTips(MatrixStack matrixStack, int mouseX, int mouseY) {
        //Water Tank tooltips
        if (Helper.isInBounds(mouseX, mouseY, guiLeft + 45, guiTop, guiLeft + 55, guiTop + 60)) {
            this.func_243308_b(matrixStack, TextHelper.createFluidTankToolTip(tile.getWaterTank()), mouseX, mouseY);
        }
        
        //Progress Bar tooltips
        if (Helper.isInBounds(mouseX, mouseY, guiLeft + 90, guiTop + 29, guiLeft + 110, guiTop + 45)) {
            this.func_243308_b(matrixStack, TextHelper.createProgressBarTooltip(tile.getProgress(), tile.getMaxProgress()), mouseX, mouseY + 15);
        }
        
        //Non Water Tank tooltips
        if (Helper.isInBounds(mouseX, mouseY, guiLeft + 115, guiTop, guiLeft + 125, guiTop + 60)) {
            this.func_243308_b(matrixStack, TextHelper.createSpecialCaseFluidTankToolTip(tile.getNonWaterFluidAmount(), tile.getMaxNonWaterFluidCapacity(), TardisConstants.ContainerComponentTranslations.MERCURY), mouseX, mouseY);
        }
        
    }
}
