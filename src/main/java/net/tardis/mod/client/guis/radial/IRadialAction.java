package net.tardis.mod.client.guis.radial;

import com.mojang.blaze3d.matrix.MatrixStack;
import net.minecraft.util.text.ITextComponent;

/** Template for Radial menu buttons. Abstracts the rendering and button press actions*/
public interface IRadialAction {
    /** Rendering Logic*/
    void render(MatrixStack matrix, int x, int y, float partialTicks);
    /** Logic to execute when the button is pressed*/
    void run();

    /** Logic to execute when the button is pressed*/
    ITextComponent getTextComponent();

}
