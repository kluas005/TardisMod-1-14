package net.tardis.mod.client.models.entity;

import net.minecraft.entity.LivingEntity;

public interface LivingArmor {
    void setLiving(LivingEntity entity);

    LivingEntity getLiving();
}
