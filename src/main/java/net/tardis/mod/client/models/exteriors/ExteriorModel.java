package net.tardis.mod.client.models.exteriors;

import com.mojang.blaze3d.matrix.MatrixStack;
import com.mojang.blaze3d.vertex.IVertexBuilder;

import net.minecraft.client.renderer.entity.model.EntityModel;
import net.minecraft.entity.Entity;
import net.tardis.mod.tileentities.exteriors.ExteriorTile;

public abstract class ExteriorModel extends EntityModel<Entity> {

	@Override
	public void setRotationAngles(Entity entityIn, float limbSwing, float limbSwingAmount, float ageInTicks,
	        float netHeadYaw, float headPitch) {
	}

	@Override
	public void render(MatrixStack matrixStackIn, IVertexBuilder bufferIn, int packedLightIn, int packedOverlayIn,
	        float red, float green, float blue, float alpha) {
	}

	public abstract void renderBones(ExteriorTile exterior, float scale, MatrixStack matrixStack, IVertexBuilder buffer, int packedLight, int packedOverlay, float alpha);
	public void renderBoti(ExteriorTile exterior, float scale, MatrixStack matrixStack, IVertexBuilder buffer, int packedLight, int packedOverlay, float alpha) {
	}
	public void render(ExteriorTile exterior, float scale, MatrixStack matrixStack, IVertexBuilder buffer, int packedLight, int packedOverlay, float alpha) {
		this.renderBones(exterior, scale, matrixStack, buffer, packedLight, packedOverlay, alpha);
		this.renderBoti(exterior, scale, matrixStack, buffer, packedLight, packedOverlay, alpha);
	}
}
