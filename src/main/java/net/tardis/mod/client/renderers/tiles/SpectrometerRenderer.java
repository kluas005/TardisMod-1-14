package net.tardis.mod.client.renderers.tiles;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.mojang.blaze3d.matrix.MatrixStack;

import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.IRenderTypeBuffer;
import net.minecraft.client.renderer.model.ItemCameraTransforms;
import net.minecraft.client.renderer.tileentity.TileEntityRenderer;
import net.minecraft.client.renderer.tileentity.TileEntityRendererDispatcher;
import net.minecraft.item.ItemStack;
import net.minecraft.util.math.vector.Vector3f;
import net.tardis.mod.client.models.LightModelRenderer;
import net.tardis.mod.helper.WorldHelper;
import net.tardis.mod.misc.WorldText;
import net.tardis.mod.tileentities.machines.NeutronicSpectrometerTile;

public class SpectrometerRenderer extends TileEntityRenderer<NeutronicSpectrometerTile> {


    public static final WorldText WORLD_TEXT = new WorldText(0.46875F, 0.28125F, 0.01F, 0xFFFFFF);

    public SpectrometerRenderer(TileEntityRendererDispatcher rendererDispatcherIn) {
        super(rendererDispatcherIn);
    }

    @Override
    public void render(NeutronicSpectrometerTile tile, float partialTicks, MatrixStack matrix, IRenderTypeBuffer bufferIn, int combinedLightIn, int combinedOverlayIn) {
        matrix.push();

        if(!tile.getSchematics().isEmpty()){
            matrix.push();

            matrix.translate(0.5, 0.5, 0.5);

            matrix.rotate(Vector3f.ZP.rotationDegrees(180));

            matrix.rotate(WorldHelper.getStandardRotationFor(tile.getBlockState()));

            matrix.translate(-0.234375, -0.421875, -0.44);
            
            Set<String> uniqueLines = new HashSet<>();
            
            for(int i = 0; i < tile.getSchematics().size(); ++i){
                uniqueLines.add(tile.getSchematics().get(i).getDisplayName());
            }
            
            List<String> lines = new ArrayList<>();
            
            uniqueLines.forEach(entry -> {
            	lines.add(entry);
            });

            WORLD_TEXT.renderText(matrix, bufferIn, LightModelRenderer.MAX_LIGHT, lines);
            matrix.pop();
        }


        ItemStack sonic = tile.getInventory().getStackInSlot(1);
        if(!sonic.isEmpty()){
            matrix.push();

            matrix.translate(0.5, 0.5, 0.5);
            matrix.rotate(Vector3f.ZP.rotationDegrees(180));
            matrix.rotate(WorldHelper.getStandardRotationFor(tile.getBlockState()));
            matrix.translate(0.34375, -0.25, 0.15625);
            matrix.scale(0.5F, 0.5F, 0.5F);
            Minecraft.getInstance().getItemRenderer().renderItem(sonic, ItemCameraTransforms.TransformType.NONE, combinedLightIn, combinedOverlayIn, matrix, bufferIn);
            matrix.pop();
        }

        matrix.scale(0.5F, 0.5F, 0.5F);
        if(!tile.getInventory().getStackInSlot(0).isEmpty()){
            matrix.translate(1, 0.5625, 1);
            matrix.rotate(Vector3f.YN.rotationDegrees(90));
            Minecraft.getInstance().getItemRenderer().renderItem(tile.getInventory().getStackInSlot(0), ItemCameraTransforms.TransformType.NONE, combinedLightIn, combinedOverlayIn, matrix, bufferIn);
        }
        matrix.pop();
    }
}
