package net.tardis.mod.texturevariants;

import net.minecraft.util.ResourceLocation;
import net.tardis.mod.Tardis;
import net.tardis.mod.helper.Helper;
import net.tardis.mod.misc.TexVariant;

public class ConsoleTextureVariants {

	public static final TexVariant[] NEMO = {
			new TexVariant(new ResourceLocation(Tardis.MODID, "textures/consoles/nemo.png"), "tardis.common.normal"),
			new TexVariant(new ResourceLocation(Tardis.MODID, "textures/consoles/nemo_ivory.png"), "console.nemo.ivory"),
			new TexVariant(new ResourceLocation(Tardis.MODID, "textures/consoles/nemo_wood.png"), "console.nemo.wood"),
			new TexVariant(Helper.createRL("textures/consoles/nemo_classic.png"), "console.nemo.classic")
	};
	
	public static final TexVariant[] STEAM = {
			new TexVariant(new ResourceLocation(Tardis.MODID, "textures/consoles/steam.png"), "tardis.common.normal"),
			new TexVariant(new ResourceLocation(Tardis.MODID, "textures/consoles/steam_ironclad.png"), "console.steam.ironclad"),
			new TexVariant(Helper.createRL("textures/consoles/steam_rosewood.png"), "console.steam.rosewood")
	};
	
	public static final TexVariant[] TOYOTA = {
			new TexVariant(new ResourceLocation(Tardis.MODID, "textures/consoles/toyota.png"), "tardis.common.normal"),
			new TexVariant(new ResourceLocation(Tardis.MODID, "textures/consoles/toyota_violet.png"), "console.toyota.violet"),
			new TexVariant(new ResourceLocation(Tardis.MODID, "textures/consoles/toyota_blue.png"), "console.toyota.blue")
	};
	
	public static final TexVariant[] GALVANIC = {
			new TexVariant(new ResourceLocation(Tardis.MODID, "textures/consoles/galvanic.png"), "tardis.common.normal"),
			new TexVariant(new ResourceLocation(Tardis.MODID, "textures/consoles/galvanic_imperial.png"), "console.galvanic.imperial"),
			new TexVariant(new ResourceLocation(Tardis.MODID, "textures/consoles/galvanic_wood.png"), "console.galvanic.wood"),
			new TexVariant(new ResourceLocation(Tardis.MODID, "textures/consoles/galvanic_sith.png"), "console.galvanic.sith")
	};

	public static final TexVariant[] XION = {
			new TexVariant(new ResourceLocation(Tardis.MODID, "textures/consoles/xion.png"), "tardis.common.normal"),
			new TexVariant(new ResourceLocation(Tardis.MODID, "textures/consoles/xion_glass.png"), "console.xion.glass"),
			new TexVariant(new ResourceLocation(Tardis.MODID, "textures/consoles/xion_sapphire.png"), "console.xion.sapphire"),
			new TexVariant(new ResourceLocation(Tardis.MODID, "textures/consoles/xion_ruby.png"), "console.xion.ruby")
	};
	
	public static final TexVariant[] CORAL = {
			new TexVariant(new ResourceLocation(Tardis.MODID, "textures/consoles/coral.png"), "tardis.common.normal"),
			new TexVariant(new ResourceLocation(Tardis.MODID, "textures/consoles/coral_blue.png"), "console.coral.blue"),
			new TexVariant(new ResourceLocation(Tardis.MODID, "textures/consoles/coral_white.png"), "console.coral.white"),
	};
	
	public static final TexVariant[] NEUTRON = {
			new TexVariant(new ResourceLocation(Tardis.MODID, "textures/consoles/neutron.png"), "tardis.common.normal"),
			new TexVariant(new ResourceLocation(Tardis.MODID, "textures/consoles/neutron_thaumic.png"), "console.neutron.thaumic"),
			new TexVariant(new ResourceLocation(Tardis.MODID, "textures/consoles/neutron_brass.png"), "console.neutron.brass")
	};
}
