package net.tardis.mod.blocks.template;

import net.minecraft.block.Block;
import net.minecraft.block.SoundType;

public class CubeBlock extends Block{

    public CubeBlock(Properties prop, SoundType sound, float hardness, float resistance) {

        super(prop.sound(sound).hardnessAndResistance(hardness, resistance));
    }

    public CubeBlock(Properties prop) {
        super(prop);
        
    }
}
