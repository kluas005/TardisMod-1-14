package net.tardis.mod.traits;

import net.minecraft.util.text.TranslationTextComponent;
import net.tardis.mod.tileentities.ConsoleTile;
import net.tardis.mod.tileentities.console.misc.PlayerTelepathicConnection;

public abstract class TardisTrait{
	
	public static final TranslationTextComponent DISLIKES_LOCATION = new TranslationTextComponent("trait.tardis.dislike_location");
	public static final TranslationTextComponent LIKES_LOCATION = new TranslationTextComponent("trait.tardis.like_location");
	
	public static final TranslationTextComponent GENERIC_LIKE = new TranslationTextComponent("trait.tardis.generic_like");
	public static final TranslationTextComponent GENERIC_DISLIKE = new TranslationTextComponent("trait.tardis.generic_dislike");
	
	private double weight = 1.0;
	private TardisTraitType type;
	
	public TardisTrait(TardisTraitType type) {
		this.type = type;
	}
	
	public double getWeight() {
		return this.weight;
	}
	
	public double getModifier() {
		return 1.0F - this.getWeight();
	}
	
	public TardisTrait setWeight(double weight) {
		this.weight = weight;
		return this;
	}
	
	public TardisTraitType getType() {
		return this.type;
	}
	
	public abstract void tick(ConsoleTile tile);
	
	public void warnPlayer(ConsoleTile tile, TranslationTextComponent text) {
		if(!tile.getWorld().isRemote()) {
			for(PlayerTelepathicConnection connection : tile.getEmotionHandler().getConnectedPlayers().values()){
				connection.getPlayer(tile.getWorld().getServer()).ifPresent(player -> {
					player.sendStatusMessage(text, true);
				});
			}
		}
	}
	
	public void warnPlayerLooped(ConsoleTile tile, TranslationTextComponent trans, int interval) {
		if(tile.getWorld().getGameTime() % interval == 0) {
			this.warnPlayer(tile, trans);
		}
	}
	
}
