package net.tardis.mod.traits;

import net.minecraft.world.biome.Biome;
import net.tardis.mod.tileentities.ConsoleTile;
import net.tardis.mod.tileentities.exteriors.ExteriorTile;

public abstract class AbstractBiomeTrait extends TardisTrait{

	private int interval;
	
	public AbstractBiomeTrait(TardisTraitType type, int interval) {
		super(type);
		this.interval = interval;
	}
	
	public AbstractBiomeTrait(TardisTraitType type) {
		this(type, 200);
	}

	@Override
	public void tick(ConsoleTile tile) {
		if(tile.getWorld().getGameTime() % this.interval == 0) {
			ExteriorTile ext = tile.getExteriorType().getExteriorTile(tile);
			if(ext != null) {
				this.onInBiomeTick(tile, ext, ext.getWorld().getBiome(ext.getPos()));
			}
		}
	}
	
	
	public abstract void onInBiomeTick(ConsoleTile console, ExteriorTile exterior, Biome biome);

}
