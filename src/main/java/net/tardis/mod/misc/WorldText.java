package net.tardis.mod.misc;

import java.util.List;

import com.mojang.blaze3d.matrix.MatrixStack;
import com.mojang.blaze3d.systems.RenderSystem;
import com.mojang.blaze3d.vertex.IVertexBuilder;

import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.FontRenderer;
import net.minecraft.client.renderer.IRenderTypeBuffer;
import net.minecraft.client.renderer.RenderType;
import net.minecraft.util.IReorderingProcessor;
import net.minecraft.util.math.MathHelper;
import net.minecraft.util.math.vector.Matrix4f;
import net.minecraft.util.text.Style;
import net.minecraft.util.text.TextFormatting;
/** Client side ONLY object to help render text in the world.*/
public class WorldText {

//    private static boolean renderBounds = false;
    public float width, height, scale;
    private int color = 0xFFFFFF;
    private boolean dropShadow = false;

    public WorldText(float width, float height, float scale, int color) {
        this.width = width;
        this.height = height;
        this.color = color;
        this.scale = scale;
    }

    public WorldText(float width, float height, float scale, TextFormatting color) {
        this(width, height, scale, color.getColor());
    }

    public void setDropShadow(boolean shadow){
        this.dropShadow = shadow;
    }

    /** Renders text in the world. <br> ONLY call this on the logical CLIENT side.*/
    public void renderText(MatrixStack matrixStack, IRenderTypeBuffer buffer, int combinedLight, boolean showRenderBounds, String... lines){

        FontRenderer fr = Minecraft.getInstance().fontRenderer;

        float offsetY = 0;

        for(int i = 0; i < lines.length; ++i){
            IReorderingProcessor processor = IReorderingProcessor.fromString(lines[i], Style.EMPTY);

            matrixStack.push();

            float scale = this.getScale(fr, lines[i]);
            matrixStack.translate(0, offsetY, 0);
            matrixStack.scale(scale, scale, scale);

            fr.drawEntityText(processor, 0, 0, this.color, this.dropShadow, matrixStack.getLast().getMatrix(), buffer, false, 0, combinedLight);

            matrixStack.pop();
            offsetY += this.scale * (fr.FONT_HEIGHT + 1);

        }
        if (showRenderBounds)
            showRenderBounds(matrixStack, buffer.getBuffer(RenderType.getLines()));

    }

    /** Renders text in the world. <br> ONLY call this on the logical CLIENT side.*/
    public void renderText(MatrixStack matrixStack, IRenderTypeBuffer buffer, int combinedLight, String... lines){
    	this.renderText(matrixStack, buffer, combinedLight, false, lines);
    }

    /** Renders text in the world. <br> ONLY call this on the logical CLIENT side.*/
    public void renderText(MatrixStack matrixStack, IRenderTypeBuffer buffer, int combinedLight, List<String> lines){
        String[] allLines = new String[lines.size()];
        lines.toArray(allLines);
    	this.renderText(matrixStack, buffer, combinedLight, allLines);
    }

    public float getScale(FontRenderer fr, String line){

        int lineWidth = fr.getStringWidth(line);
        float scale = this.width / (float)lineWidth;
        scale = MathHelper.clamp(scale, 0, this.scale);

        return scale;
    }

    private void showRenderBounds(MatrixStack matrixStack, IVertexBuilder builder) {
        RenderSystem.disableTexture();
        MatrixStack.Entry matrixstackEntry = matrixStack.getLast();
	    Matrix4f matrix4f = matrixstackEntry.getMatrix();

        builder.pos(matrix4f, 0, 0, 0).color(1F, 0, 0, 1F).endVertex();
        builder.pos(matrix4f, width, 0, 0).color(1F, 0, 0, 1).endVertex();

        builder.pos(matrix4f, 0, height, 0).color(1F, 0, 0, 1F).endVertex();
        builder.pos(matrix4f, width, height, 0).color(1F, 0, 0, 1).endVertex();

        builder.pos(matrix4f, 0, 0, 0).color(0, 1F, 0, 1F).endVertex();
        builder.pos(matrix4f, 0, height, 0).color(0, 1F, 0, 1F).endVertex();

        builder.pos(matrix4f, width, 0, 0).color(0, 1F, 0, 1F).endVertex();
        builder.pos(matrix4f, width, height, 0).color(0, 1F, 0, 1F).endVertex();

        RenderSystem.enableTexture();
    }
}
