package net.tardis.mod.network.packets.exterior;

import net.minecraft.network.PacketBuffer;
import net.tardis.mod.tileentities.exteriors.ExteriorTile;

public class CrashedData extends ExteriorData {

    boolean crashed;

    public CrashedData(boolean crashed) {
        super(Type.CRASHED);
        this.crashed = crashed;
    }
    public CrashedData(Type type) {
        super(type);
    }

    @Override
    public void encode(PacketBuffer buf) {
        buf.writeBoolean(this.crashed);
    }

    @Override
    public void decode(PacketBuffer buf) {
        this.crashed = buf.readBoolean();
    }

    @Override
    public void apply(ExteriorTile object) {
        object.setCrashed(this.crashed);
    }
}
