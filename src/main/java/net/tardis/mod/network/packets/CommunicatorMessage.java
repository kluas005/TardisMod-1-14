package net.tardis.mod.network.packets;

import java.util.function.Supplier;

import net.minecraft.network.PacketBuffer;
import net.minecraft.util.RegistryKey;
import net.minecraft.util.registry.Registry;
import net.minecraft.world.DimensionType;
import net.minecraftforge.fml.network.NetworkEvent;
import net.tardis.mod.helper.TardisHelper;
import net.tardis.mod.misc.SpaceTimeCoord;

public class CommunicatorMessage {

	Type accepted;
	
	public CommunicatorMessage(Type accepted) {
		this.accepted = accepted;
	}
	
	public static void encode(CommunicatorMessage mes, PacketBuffer buf) {
		buf.writeInt(mes.accepted.ordinal());
	}
	
	public static CommunicatorMessage decode(PacketBuffer buf) {
		return new CommunicatorMessage(Type.values()[buf.readInt()]);
	}
	
	public static void handle(CommunicatorMessage mes, Supplier<NetworkEvent.Context> cont) {
		cont.get().enqueueWork(() -> {
			TardisHelper.getConsoleInWorld(cont.get().getSender().world).ifPresent(tile -> {
				if(!tile.getDistressSignals().isEmpty()) {
					if(mes.accepted == Type.ACCEPT) {
						SpaceTimeCoord pos = tile.getDistressSignals().get(0).getSpaceTimeCoord();
						tile.setDestination(RegistryKey.getOrCreateKey(Registry.WORLD_KEY, pos.getDimRL()), pos.getPos());
						tile.getDistressSignals().remove(0);
					}
					else if(mes.accepted == Type.IGNORE_ALL)
						tile.getDistressSignals().clear();
					else tile.getDistressSignals().remove(0);
					tile.updateClient();
				}
			});
				
		});
		cont.get().setPacketHandled(true);
	}
	
	public static enum Type{
		ACCEPT,
		IGNORE,
		IGNORE_ALL
	}
}
