package net.tardis.mod.containers;

import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.inventory.container.ContainerType;
import net.minecraft.network.PacketBuffer;
import net.minecraftforge.items.SlotItemHandler;
import net.tardis.mod.helper.TInventoryHelper;
import net.tardis.mod.tileentities.machines.QuantiscopeTile;

public class QuantiscopeWeldContainer extends QuantiscopeContainer{
	
	protected QuantiscopeWeldContainer(ContainerType<?> type, int id) {
		super(type, id);
	}
	/** Client Only constructor */
	public QuantiscopeWeldContainer(int id, PlayerInventory inv, PacketBuffer buf) {
		super(TContainers.QUANTISCOPE_WELD.get(), id);
		this.init(inv, (QuantiscopeTile)inv.player.world.getTileEntity(buf.readBlockPos()));
	}
	/** Server Only constructor */
	public QuantiscopeWeldContainer(int id, PlayerInventory inv, QuantiscopeTile tile) {
		super(TContainers.QUANTISCOPE_WELD.get(), id);
		this.init(inv, tile);
	}
	
	public void init(PlayerInventory inv, QuantiscopeTile quantiscope) {
		
		blockEntity = quantiscope;
		
		//Parts
		this.addSlot(new SlotItemHandler(quantiscope, 0, 50, 9));
		this.addSlot(new SlotItemHandler(quantiscope, 1, 29, 9));
		this.addSlot(new SlotItemHandler(quantiscope, 2, 16, 29));
		this.addSlot(new SlotItemHandler(quantiscope, 3, 29, 49));
		this.addSlot(new SlotItemHandler(quantiscope, 4, 50, 49));
		
		//Thing to repair
		this.addSlot(new SlotItemHandler(quantiscope, 5, 64, 29));
		//Repaired Item
		this.addSlot(new SlotItemHandler(quantiscope, 6, 120, 29));
		
		//Player Inv
	    TInventoryHelper.addPlayerInvContainer(this, inv, 0, -2);
	     
	}
}
