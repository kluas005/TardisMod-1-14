package net.tardis.mod.enums;

public enum EnumDoorState {

    CLOSED,
    ONE,
    BOTH;
}
