package net.tardis.mod.helper;

import net.minecraft.util.ResourceLocation;
import net.minecraftforge.fml.ModLoadingContext;
import net.minecraftforge.registries.IForgeRegistry;
import net.minecraftforge.registries.IForgeRegistryEntry;

public class RegisterObjects<T extends IForgeRegistryEntry<T>> {
	/**
	 * Convenience Method to register objects through IForgeRegistry
	 * Originally used to help with WorldGen, may be useful in future? 
	 */
	protected final IForgeRegistry<T> registry;
	private final String modId;
	
	public RegisterObjects(IForgeRegistry<T> registry) {
	   this(registry, ModLoadingContext.get().getActiveContainer().getModId());
	 }

	 public RegisterObjects(IForgeRegistry<T> registry, String modId) {
	   this.registry = registry;
	   this.modId = modId;
	}
	
	  public <I extends T> I register(I forgeRegitryEntry, String name) {
		    return this.register(forgeRegitryEntry, this.getResource(name));
	  }
	  
	  public <I extends T> I register(I forgeRegistryEntry, ResourceLocation location) {
		    forgeRegistryEntry.setRegistryName(location);
		    registry.register(forgeRegistryEntry);
		    return forgeRegistryEntry;
	  }
	  
	  public ResourceLocation getResource(String name) {
		    return new ResourceLocation(modId, name);
	  }
}
