package net.tardis.mod.helper;

import java.util.List;
import java.util.Optional;

import javax.annotation.Nullable;

import net.minecraft.block.BlockState;
import net.minecraft.block.material.Material;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.tags.FluidTags;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.Direction;
import net.minecraft.util.RegistryKey;
import net.minecraft.util.SoundCategory;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.ChunkPos;
import net.minecraft.world.World;
import net.minecraft.world.gen.Heightmap;
import net.minecraft.world.server.ServerWorld;
import net.tardis.mod.cap.Capabilities;
import net.tardis.mod.constants.TardisConstants;
import net.tardis.mod.controls.LandingTypeControl.EnumLandType;
import net.tardis.mod.exterior.AbstractExterior;
import net.tardis.mod.misc.ObjectWrapper;
import net.tardis.mod.misc.SpaceTimeCoord;
import net.tardis.mod.sounds.TSounds;
import net.tardis.mod.subsystem.ShieldGeneratorSubsystem;
import net.tardis.mod.tileentities.ConsoleTile;
import net.tardis.mod.tileentities.IAffectTARDISLanding;
import net.tardis.mod.tileentities.console.misc.AlarmType;
import net.tardis.mod.tileentities.console.misc.MonitorOverride;
import net.tardis.mod.tileentities.exteriors.ExteriorTile;
import net.tardis.mod.tileentities.machines.TransductionBarrierTile;

public class LandingSystem {
	
	
	public static boolean isSafe(World world, BlockPos pos) {
		return world.getBlockState(pos).getMaterial().isReplaceable();
	}
	
	public static boolean canLand(World world, BlockPos pos, AbstractExterior exterior, ConsoleTile console) {
		BlockPos up = new BlockPos(exterior.getWidth(console), exterior.getHeight(console), exterior.getWidth(console));
		BlockPos down = new BlockPos(exterior.getWidth(console), 0, exterior.getWidth(console));
		for(BlockPos check : BlockPos.getAllInBoxMutable(pos.subtract(down), pos.add(up))){
			if(!isSafe(world, check))
				return false;
		}
		return world.getBlockState(pos.down()).isSolid();
	}
	
	public static BlockPos findValidLandSpot(World world, BlockPos dest, EnumLandType type, AbstractExterior ext, ConsoleTile console) {
		
		if(canLand(world, dest, ext, console))
			return dest;
		
		if(type == EnumLandType.UP) {
			return getLandSpotUp(world, dest, ext, console);
		}
		
		if(type == EnumLandType.DOWN) {
			return getLandSpotDown(world, dest, ext, console);
		}
		
		return BlockPos.ZERO;
		
	}
	
	public static BlockPos getLandSpotUp(World world, BlockPos dest, AbstractExterior ext, ConsoleTile console) {
		for(int y = dest.getY(); y < calculateHeight(world, console, ext); ++y) {
			BlockPos test = new BlockPos(dest.getX(), y, dest.getZ());
			if(canLand(world, test, ext, console))
				return test;
		}
		return BlockPos.ZERO;
	}
	
	public static BlockPos getLandSpotDown(World world, BlockPos dest, AbstractExterior ext, ConsoleTile console) {
		for(int y = dest.getY(); y > 0; --y) {
			BlockPos test = new BlockPos(dest.getX(), y, dest.getZ());
			if(canLand(world, test, ext, console))
				return test;
		}
		return BlockPos.ZERO;
	}
	
	/**
	 * Searches for a suitable position that isn't above the Nether Roof
	 * NOT SAFE FOR TARDISES
	 * @implNote This is better than using world heightmap, because on worlds with a bedrock roof, it will allow you to get past the roof
	 * @param world
	 * @param pos
	 * @return
	 */
	public static BlockPos getTopBlock(World world, BlockPos pos) {
		for(int y = world.getHeight(); y > 0; --y) {
			BlockPos newPos = new BlockPos(pos.getX(), y, pos.getZ());
			BlockState underState = world.getBlockState(newPos.down());
			
			if(!willBlockStateCauseSuffocation(world, newPos) && underState.isSolid() && !isPosBelowOrAboveWorld(world, newPos.getY())) {
				return newPos;
			}
		}
		return pos;
	}
	
	/**
	 * Checks if BlockPos is within World Border and not beyond it
	 * @param world
	 * @param pos
	 * @return true if within border, false if beyond border
	 */
	public static boolean isBlockWithinWorldBorder(World world, BlockPos pos) {
		return world.getWorldBorder().contains(pos);
	}
	
	/**
	 * Checks if BlockPos is within World Border and not beyond it
	 * @param world
	 * @param x
	 * @param y
	 * @param z
	 * @return true if within border, false if beyond border
	 */
	public static boolean isBlockWithinWorldBorder(World world, int x, int y, int z) {
		return world.getWorldBorder().contains(new BlockPos(x,y,z));
	}
	
	public static BlockPos getLand(World world, BlockPos dest, EnumLandType type, ConsoleTile console) {
		BlockPos pos = findValidLandSpot(world, dest, type, console.getExteriorType(), console);
		if(pos.equals(BlockPos.ZERO))
			pos = findValidLandSpot(world, dest, type == EnumLandType.DOWN ? EnumLandType.UP : EnumLandType.DOWN, console.getExteriorType(), console);
		return pos;
	}
	/**
	 * Dimension Specific version of the check, prevents users from teleporting to the Nether roof
	 * DO NOT USE FOR BLOCKS, ONLY FOR ENTITIES
	 * @implNote If returns false, will search for safe spot that is not above the Nether roof
	 * @param dim
	 * @param y
	 * @return
	 */
	public static boolean isPosBelowOrAboveWorld(World dim, int y) {
		if (dim.getDimensionKey() == World.THE_NETHER) {
			return y <= 0 || y >= 126;
		}
		return y <= 0 || y >= dim.getHeight();
	}
	
	/**
	 * Checks if the blockstate will cause entity to suffocate in it.
	 * <br> This attempts to reproduce behaviour shown in the private AbstractBlock suffocates predicate
	 * <br> Do not use AbstractBlock#causesSuffocation because that does not actually relate to suffocation at all, it is a rendering related method misnamed in 1.16 mappings 
	 * @param world
	 * @param pos
	 * @return true if causes suffocating, false if it doesn't
	 */
    public static boolean willBlockStateCauseSuffocation(World world, BlockPos pos) {
    	BlockState state = world.getBlockState(pos);
    	return state.getMaterial().blocksMovement() && state.hasOpaqueCollisionShape(world, pos);
    }
    
	public static BlockPos validateBlockPos(BlockPos pos, World world, ConsoleTile tile, AbstractExterior exterior) {
    	int maxHeight = calculateHeight(world, tile, exterior);
		if(pos.getY() < 0)
			return new BlockPos(pos.getX(), 0, pos.getZ());
		if(pos.getY() >= maxHeight) {
			if (pos.getY() >= world.getHeight()) //Do another validation to make sure we're still below the actual world height, as sometimes the calculated "max height" may still be above actual world height due to different exterior sizes
				return new BlockPos(pos.getX(), world.getHeight() - 1, pos.getZ());
			return new BlockPos(pos.getX(), maxHeight - 1, pos.getZ()); //Move one block below max height so that the exterior blocks are not being placed above world height. (Max height is 256, but build limit is actually 255)
		}
		return pos;
	}

	/**
	 *
	 * @param world
	 * @param pos - Block Below the exterior
	 * @return True if the TARDIS should turn into an entity and fall
	 */
	public static boolean shouldTARDISFall(World world, BlockPos pos){

		ObjectWrapper<Boolean> hasGrav = new ObjectWrapper<Boolean>(true);
		world.getCapability(Capabilities.SPACE_DIM_PROPERTIES).ifPresent(cap -> {
			if(cap.isZeroG())
				hasGrav.setValue(false);
		});

		if(!hasGrav.getValue())
			return false;

		return (world.getBlockState(pos).getCollisionShape(world, pos).isEmpty() || !world.getBlockState(pos).isSolid()) && world.getBlockState(pos).getMaterial() == Material.AIR;
	}

	public static BlockPos tryToKill(World world, BlockPos pos, Direction facing, int range){
		Optional<BlockPos> landPos = BlockPos.getAllInBox(new AxisAlignedBB(pos).grow(range))
				.filter(p -> isSafe(world, p))
				.filter(p ->{
					return isDanger(world.getBlockState(p.offset(facing))) || isDanger(world.getBlockState(p.offset(facing).down()));
				}).findAny();
		return landPos.orElse(pos);
	}

	public static boolean isDanger(BlockState state){
		return state.getFluidState().isTagged(FluidTags.LAVA);
	}

    public static BlockPos redirectBehindBorder(ServerWorld world, BlockPos destination, ConsoleTile console) {
		BlockPos finalDestination = destination;
		if(!world.getWorldBorder().contains(destination)) {
			BlockPos pos = world.getSpawnPoint();
			if(pos == null)
				pos = BlockPos.ZERO;
			finalDestination = console.randomizeCoords(world.getHeight(Heightmap.Type.MOTION_BLOCKING_NO_LEAVES, pos), 50);
			for(PlayerEntity ent : console.getWorld().getEntitiesWithinAABB(PlayerEntity.class, new AxisAlignedBB(console.getPos()).grow(30))) {
				ent.sendStatusMessage(TardisConstants.Translations.OUTSIDE_BORDER, true);
			}
			console.getInteriorManager().soundAlarm(AlarmType.LOW);
			console.getInteriorManager().setMonitorOverrides(new MonitorOverride(console, 100, TardisConstants.Translations.OUTSIDE_BORDER.getString()));
		}
		return finalDestination;
    }

	/**
	 * Returns null if this isn't going inside of a TARDIS
	 * @param serverWorld
	 * @param destination
	 * @param console
	 * @return
	 */
    @Nullable
	public static SpaceTimeCoord handleTardisInTardis(ServerWorld serverWorld, BlockPos destination, ConsoleTile console) {

		//If there is a TARDIS under this one
		TileEntity te = serverWorld.getTileEntity(destination.down());
		if(te instanceof ExteriorTile) {
			ExteriorTile other = (ExteriorTile)te;

			//If not our own
			if(other.getInteriorDimensionKey() != console.getWorld().getDimensionKey()) {
				RegistryKey<World> otherTardisWorldInteriorKey = other.getInteriorDimensionKey();
				ServerWorld otherTardisWorld = console.getWorld().getServer().getWorld(otherTardisWorldInteriorKey);
				//Check if its shields are down
				ConsoleTile otherConsole = TardisHelper.getConsoleInWorld(otherTardisWorld).orElse(null);
				if(otherConsole != null) {
					ShieldGeneratorSubsystem sys = otherConsole.getSubsystem(ShieldGeneratorSubsystem.class).orElse(null);
					if(sys == null || !sys.canBeUsed()) {
						return new SpaceTimeCoord(otherTardisWorldInteriorKey, LandingSystem.getLand(serverWorld, console.randomizeCoords(new BlockPos(0, 128, 0), 10), EnumLandType.DOWN, console));
					}
					else if(sys != null)
						sys.damage(null, 1);
				}
			}
		}

		return null;
	}

	/**
	 * Any dimension returned is discarded, only change the position and facing here please
	 * @param serverWorld
	 * @param pos
	 * @return - null if unchanged
	 */
	@Nullable
	public static SpaceTimeCoord affectFromWorldBlocks(ServerWorld serverWorld, BlockPos pos, EnumLandType type, ConsoleTile console) {
		List<TileEntity> possibleHazards = WorldHelper.getTEsInChunks(serverWorld, new ChunkPos(pos), 3);
		possibleHazards.removeIf(te -> !(te instanceof IAffectTARDISLanding));

		//Look for tiles that effect TARDIS Landing
		for(TileEntity te : possibleHazards) {
			IAffectTARDISLanding affect = (IAffectTARDISLanding) te;
			if(pos.withinDistance(te.getPos(), affect.getEffectiveRange())){
				SpaceTimeCoord coord = affect.affectTARDIS(serverWorld, new SpaceTimeCoord(serverWorld.getDimensionKey(), pos, console.getExteriorFacingDirection()), console);
				if(coord != null) {
					if (te instanceof TransductionBarrierTile) {
						console.getWorld().playSound(null, console.getPos(), TSounds.CANT_START.get(), SoundCategory.BLOCKS, 1F, 1F);
					}
					return coord;
				}
			}
		}
		return null;
	}

	public static int calculateHeight(World world, ConsoleTile tile, AbstractExterior exterior){
		return world.getHeight() - exterior.getHeight(tile) + 1;
	}

}
