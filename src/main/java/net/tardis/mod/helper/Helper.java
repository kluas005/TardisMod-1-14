package net.tardis.mod.helper;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.function.Function;

import javax.annotation.Nullable;

import com.google.common.collect.Lists;

import net.minecraft.advancements.Advancement;
import net.minecraft.block.BlockState;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.ServerPlayerEntity;
import net.minecraft.nbt.INBT;
import net.minecraft.nbt.ListNBT;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.IServerWorld;
import net.minecraftforge.fml.common.ObfuscationReflectionHelper;
import net.tardis.mod.Tardis;
import net.tardis.mod.config.TConfig;
import net.tardis.mod.constants.TardisConstants;
import net.tardis.mod.misc.TexVariant;
import net.tardis.mod.tileentities.ConsoleTile;
import net.tardis.mod.tileentities.ShipComputerTile;
/** Generic Helper class*/
public class Helper {

    public static Random rand = new Random();

	public static boolean isInBounds(int testX, int testY, int x, int y, int u, int v) {
		return (testX > x &&
				testX < u &&
				testY > y &&
				testY < v);
	}

    public static void doIfAdvancementPresent(String name, ServerPlayerEntity ent, Runnable run) {
    	Advancement adv = ent.getServer().getAdvancementManager().getAdvancement(new ResourceLocation(Tardis.MODID, name));
    	if(adv != null) {
    		if(ent.getAdvancements().getProgress(adv).isDone()) {
    			run.run();
    		}
    	}
    }
    /**
     * Run if a specified advancement has been obtained (Works for other mods or Vanilla MC)
     * @param name
     * @param ent
     * @param run
     */
    public static void doIfAdvancementPresentOther(ResourceLocation loc, ServerPlayerEntity ent, Runnable run) {
    	Advancement adv = ent.getServer().getAdvancementManager().getAdvancement(loc);
    	if(adv != null) {
    		if(ent.getAdvancements().getProgress(adv).isDone()) {
    			run.run();
    		}
    	}
    }

	public static ResourceLocation createRL(String string) {
		return new ResourceLocation(Tardis.MODID, string);
	}
	
	public static String createRLString(String name) {
    	return Tardis.MODID + ":" + name;
    }
	
	public static ResourceLocation createManualRL(String string) {
		return new ResourceLocation(Tardis.MODID, "manual/" + string);
	}
	
	public static ResourceLocation createOldManualRL(String string) {
		return new ResourceLocation(Tardis.MODID, "manual/chapters/" + string + ".json");
	}
	
	/** Gets a string based translation key based on a subsystem entry's registry name
	 * <br> Used for systems like protocols to send a status message if the required subsystem is not activated
	 * <br> E.g. Forcefield needs shield to be activated*/
    public static String getSubSystemNotActivatedMessage(String name) {
    	return "message." + Tardis.MODID + ".subsystem.not_activated." + name;
    }
	
	/**
	 * Tests if a blockstate is blacklisted from rendering in BOTI applications
	 * @param state
	 * @return false if blacklisted, true if allowed
	 */
	public static boolean canRenderInBOTI(BlockState state) {
		for(String blocked : TConfig.CLIENT.botiBlacklistedBlocks.get()) {
			if(state.getBlock().getRegistryName().toString().equals(blocked))
				return false;
			if(blocked.endsWith("*")) {
				String modid = blocked.substring(0, blocked.indexOf(':'));
				if(state.getBlock().getRegistryName().getNamespace().equals(modid))
					return false;
			}
		}
		return true;
	}
	
	/**
     * Tests if an entity is blacklisted from rendering in BOTI applications
     * @param entity
     * @return false if blacklisted, true if allowed
     */
    public static boolean canRenderInBOTI(Entity entity) {

    	if(entity == null)
    		return true;

        for(String blocked : TConfig.CLIENT.botiBlacklistedEntities.get()) {
            if(entity.getType().getRegistryName().toString().equals(blocked))
                return false;
            if(blocked.endsWith("*")) {
                String modid = blocked.substring(0, blocked.indexOf(':'));
                if(entity.getType().getRegistryName().getNamespace().equals(modid))
                    return false;
            }
        }
        return true;
    }

	public static <T extends Object> void addAllToListNotDuplicate(ArrayList<T> list1, List<T> list2) {
		for(T obj : list2) {
			if(!list1.contains(obj))
				list1.add(obj);
		}
	}

	public static <T extends INBT> List<T> getListFromNBT(ListNBT nbt, Class<T> nbtClass) {
		List<T> list = Lists.newArrayList();
		for(INBT n : nbt) {
			list.add(nbtClass.cast(n));
		}
		return list;
	}

	public static String[] getConsoleText(ConsoleTile console) {
		
		if(console.getFlightEvent() != null)
			return new String[] {
					"",
					console.getFlightEvent().getTranslation().getString()
					};
		
		if(console.getInteriorManager().getMonitorOverrides() != null)
			return console.getInteriorManager().getMonitorOverrides().getText();
		
		boolean hasNav = console.hasNavCom();
		
		return new String[] {
		        TardisConstants.Translations.LOCATION.getString() + (hasNav ? WorldHelper.formatBlockPos(console.getCurrentLocation()): TardisConstants.Translations.UNKNOWN.getString()),
				TardisConstants.Translations.DIMENSION.getString() + (hasNav ? WorldHelper.formatDimName(console.getCurrentDimension()) : TardisConstants.Translations.UNKNOWN.getString()),
				TardisConstants.Translations.FACING.getString() + console.getExteriorFacingDirection().getName2().toUpperCase(),
				TardisConstants.Translations.TARGET.getString() + (hasNav ? WorldHelper.formatBlockPos(console.getDestinationPosition()) : TardisConstants.Translations.UNKNOWN.getString()),
				TardisConstants.Translations.TARGET_DIM.getString() + (hasNav ? WorldHelper.formatDimName(console.getDestinationDimension()) : TardisConstants.Translations.UNKNOWN.getString()),
				TardisConstants.Translations.ARTRON.getString() + TardisConstants.TEXT_FORMAT_NO_DECIMALS.format(console.getArtron()) + "AU",
				TardisConstants.Translations.JOURNEY.getString() + TardisConstants.TEXT_FORMAT_NO_DECIMALS.format(console.getPercentageJourney() * 100.0F) + "%"
				};
	}

	public static AxisAlignedBB createBBWithRaduis(int i) {
		return new AxisAlignedBB(-i, -i, -i, i, i, i);
	}
	
	// helper for making the above private field getters via reflection
	@SuppressWarnings("unchecked") // also throws ClassCastException if the types are wrong
	static <FIELDHOLDER,FIELDTYPE> Function<FIELDHOLDER,FIELDTYPE> getInstanceField(Class<FIELDHOLDER> fieldHolderClass, String fieldName)
	{
		// forge's ORH is needed to reflect into vanilla minecraft java
		Field field = ObfuscationReflectionHelper.findField(fieldHolderClass, fieldName);
		
		return instance -> {
			try
			{
				return (FIELDTYPE)(field.get(instance));
			}
			catch (IllegalArgumentException | IllegalAccessException e)
			{
				throw new RuntimeException(e);
			}
		};
	}
	
	public static void addLootToComputerBelow(IServerWorld worldIn, BlockPos pos, ResourceLocation lootRL) {
		TileEntity te = worldIn.getTileEntity(pos.down());
		if (te instanceof ShipComputerTile) {
			 ShipComputerTile computer = (ShipComputerTile) te;
			 computer.setLootTable(lootRL);
			 worldIn.removeBlock(pos, false);
		}
	}
	
	public static void addSchematicToComputerBelow(IServerWorld worldIn, BlockPos pos, ResourceLocation schematic) {
		TileEntity te = worldIn.getTileEntity(pos.down());
		if (te instanceof ShipComputerTile) {
			 ShipComputerTile computer = (ShipComputerTile) te;
			 computer.setSchematic(schematic);
			 worldIn.removeBlock(pos, false);
		}
	}

    public static float getConfigPercent(int configInput) {
	    float numerator = (float)configInput;
	    float denominator = 100F;
	    float result = numerator / denominator;
	    return result;
    }

    public static ResourceLocation getVariantTextureOr(@Nullable TexVariant variant, ResourceLocation defaultTex) {
		if(variant == null || variant.getTexture() == null){
			return defaultTex;
		}
		return variant.getTexture();
    }

	public static int[] toIntArray(List<? extends Integer> list){
        int[] array = new int[list.size()];
	    for(int i = 0; i < array.length; i++)
	    	array[i] = list.get(i);
	    return array;

    }
	public static float getChunkOffset(float val, float size) {
		return val >= 0 ? val / size : (val / size) - 1;
	}

}