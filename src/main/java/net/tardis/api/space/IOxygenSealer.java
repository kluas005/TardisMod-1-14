package net.tardis.api.space;

import net.minecraft.nbt.INBT;
import net.minecraft.util.Direction;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.common.capabilities.ICapabilityProvider;
import net.minecraftforge.common.util.LazyOptional;
import net.tardis.mod.cap.Capabilities;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.List;
/** Interface that allows an object to define areas where oxgyen can be stored without it escaping
 * <br> For an example, see {@linkplain net.tardis.mod.tileentities.machines.OxygenSealTile OxygenSealTile} */
public interface IOxygenSealer {

    List<BlockPos> getSealedPositions();
    void setSealedPositions(List<BlockPos> list);

    void tick(World world, BlockPos pos);

    public static class Storage implements Capability.IStorage<IOxygenSealer>{

        @Nullable
        @Override
        public INBT writeNBT(Capability<IOxygenSealer> capability, IOxygenSealer instance, Direction side) {
            return null;
        }

        @Override
        public void readNBT(Capability<IOxygenSealer> capability, IOxygenSealer instance, Direction side, INBT nbt) {

        }
    }

    public static class Provider implements ICapabilityProvider {

        private LazyOptional<IOxygenSealer> sealerOptional;
        private IOxygenSealer sealer;

        public Provider(IOxygenSealer sealer){
            this.sealer = sealer;
            this.sealerOptional = LazyOptional.of(() -> this.sealer);
        }

        @Nonnull
        @Override
        public <T> LazyOptional<T> getCapability(@Nonnull Capability<T> cap, @Nullable Direction side) {
            return cap == Capabilities.OXYGEN_SEALER ? this.sealerOptional.cast() : LazyOptional.empty();
        }
    }
}
